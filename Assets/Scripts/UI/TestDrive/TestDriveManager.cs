using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TestDriveManager : MonoBehaviour
{
    [SerializeField] private GameObject[] TestDriveTracks;
    [SerializeField] private GameObject RaceUI, TestDriveUI;
    [SerializeField] private Image TestDriveTypeIcon;
    [SerializeField] private Sprite[] TestDriveTypeSprites;
    [SerializeField] private Transform TestDrivePanelsHolder;
    private List<TestDrivePanelData> testDrivePanels = new List<TestDrivePanelData>();

    public static TestDriveManager instance;

    public static int TrackNum = -1;

    public static bool wasInTestDrive;

    RaceSystem raceSystem => RaceSystem.instance;

    public void StartTestDrive(int trackNum)
    {
        TrackNum = trackNum;
        raceSystem.SetTrack(Instantiate(TestDriveTracks[TrackNum]).GetComponent<PathActivator>());
        SetTestDriveUI();
        raceSystem.StartRace();
    }

    public void ExitTestDrive()
    {
        wasInTestDrive = true;
        TrackNum = -1;
        SceneManager.LoadScene("Game");
    }

    private void SetTestDriveUI()
    {
        RaceUI.SetActive(false);
        TestDriveTypeIcon.sprite = TestDriveTypeSprites[TrackNum];
        TestDriveUI.SetActive(true);
    }

    public void Init()
    {
        instance = this;

        for(int i = 0; i< TestDrivePanelsHolder.childCount; i++)
        {
            TestDrivePanelData testDrivePanelData = TestDrivePanelsHolder.GetChild(i).GetComponent<TestDrivePanelData>();
            if(testDrivePanelData != null)
            {
                testDrivePanelData.SetBestTimeText(testDrivePanels.Count);
                testDrivePanels.Add(testDrivePanelData);
            }
        }
    }
}
