using UnityEngine;
using UnityEngine.UI;

public class TestDrivePanelData : MonoBehaviour
{
    [SerializeField] private Text BestTimeTxt;

    public void SetBestTimeText(int panelNum)
    {
        float bestTime = PlayerPrefs.GetFloat("BestTestDriveTime" + panelNum.ToString(), 10000);
        if (bestTime > 5000) bestTime = 0;
        BestTimeTxt.text = "ЛУЧШЕЕ ВРЕМЯ: " + UISystem.GetTimeWithSeconds(bestTime);
    }
    
}
