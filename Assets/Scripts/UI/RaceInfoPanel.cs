using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RaceInfoPanel : MonoBehaviour
{
    [SerializeField] private Image[] CarImgs;
    [SerializeField] private Color _gray;

    public void SelectCar(int id)
    {
        foreach(Image img in CarImgs)
        {
            img.color = _gray;
        }
        CarImgs[id].color = Color.white;
    }
}
