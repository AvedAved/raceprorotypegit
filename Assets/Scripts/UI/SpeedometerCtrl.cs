using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpeedometerCtrl : MonoBehaviour
{
    private RaceSystem raceSystem => RaceSystem.instance;

    [SerializeField] private float minArrowAngle;
    private float maxArrowAngle = -90;

    [SerializeField] private RectTransform speedArrow;
    [SerializeField] private Text speedTxt;
    [SerializeField] private Image brakeIndicator;

   

    public void SpeedUpdate()
    {
        speedTxt.text = ((int)raceSystem.playerCar.currentSpeed).ToString() + " km/h" ;
        speedArrow.localEulerAngles = new Vector3(0, 0, Mathf.Lerp(minArrowAngle,maxArrowAngle, raceSystem.playerCar.currentSpeed / raceSystem.playerCar.carParameters.maxGameSpeed));
    }

    public void SetBrakeIndicatorColor(float alpha)
    {
        brakeIndicator.color = new Color(brakeIndicator.color.r,brakeIndicator.color.g,brakeIndicator.color.b,alpha);
    }
}
