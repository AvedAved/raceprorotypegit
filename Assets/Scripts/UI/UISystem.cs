using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UISystem : MonoBehaviour
{
    public static UISystem instance;
    private RaceSystem raceSystem => RaceSystem.instance;

    [SerializeField] private GarageSystem garageSystem;
    [SerializeField] private TestDriveManager testDriveManager;

    [SerializeField] private GameObject[] MenuUI,GameUI;
    [SerializeField] private GameObject PauseBut, ContinueBut,SkipBut;
    [SerializeField] private GameObject GaragePan;
    [SerializeField] private GameObject RaceInfoPanel;
    [SerializeField] private FinishPanelStruct RaceFinishPan, TestDriveFinishPan;

    [Header("Texts")]
    [SerializeField] private Text CurrentLapTxt;
    [SerializeField] private Text PlayerRacePosTxt;
    public Text WeatherTxt;
    public Text LapsAmountTxt;
    public Text SurfaceTxt;

    [Header("Images")]
    [SerializeField] private Image RaceInfoImg;
    [SerializeField] private Sprite[] RaceInfoSprites;

    private void Awake()
    {
        instance = this;
        garageSystem.Init();
        testDriveManager.Init();
    }

    private void Start()
    {
        TutorialManager.instance.StartTutorial(TutorialSpecialType.FirstGameSceneLoad);
    }

    public void OpenFinishPanel(int position, float time, bool testDrive)
    {
        if (!testDrive)
        {
            RaceFinishPan.playerPosTxt.text = $"ПОЗИЦИЯ: {position} / 4";
            RaceFinishPan.raceTime.text = $"ВРЕМЯ: {GetTimeWithSeconds(time)}"; 
            RaceFinishPan.panel.SetActive(true);
        }
        else
        {
            TestDriveFinishPan.raceTime.text = $"ВРЕМЯ: {GetTimeWithSeconds(time)}";
            TestDriveFinishPan.panel.SetActive(true);
        }
    }

    public void LapTxtUpdate(int currentLap)
    {
        CurrentLapTxt.text = currentLap.ToString() + " / " + PathActivator.instance.lapsAmount.ToString();
    }

    public void PlayerRacePosTxtUpdate(int currentPos, int totalCars = 4)
    {
        PlayerRacePosTxt.text = $"{currentPos} / {totalCars}";
    }

    public void SkipRace(int x)
    {
        Time.timeScale = x;
        PauseBut.SetActive(false);
        ContinueBut.SetActive(false);
        SkipBut.SetActive(false);
    }
    public void PauseRace()
    {
        Time.timeScale = 0;
        PauseBut.SetActive(false);
        ContinueBut.SetActive(true);
    }
    public void ContinueRace()
    {
        Time.timeScale = 1;
        PauseBut.SetActive(true);
        ContinueBut.SetActive(false);
    }
    public void MenuUIActive(bool active)
    {
        foreach(GameObject go in MenuUI)
        {
            go.SetActive(active);
        }
    }
    public void GameUIActive(bool active)
    {
        foreach (GameObject go in GameUI)
        {
            go.SetActive(active);
        }
    }

    public void OpenGarage()
    {
        garageSystem.DetailPanelsExit();
        testDriveManager.gameObject.SetActive(false);
        GaragePan.SetActive(!GaragePan.activeSelf);
        if (GaragePan.activeSelf)
        {
            TutorialManager.instance.StartTutorial(TutorialSpecialType.FirstGarageOpen);
        }
    }

    public void OpenAIParameters()
    {
        RaceInfoPanel.SetActive(!RaceInfoPanel.activeSelf);
        RaceInfoImg.sprite = RaceInfoSprites[RaceInfoPanel.activeSelf ? 1 : 0];
    }

    public void OpenTestDrivePanelAtStart()
    {
        GaragePan.SetActive(true);
        garageSystem.MainPanel.SetActive(false);
        testDriveManager.gameObject.SetActive(true);
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    public static string GetTimeWithSeconds(float sec)
    {
        float minutes = sec / 60;
        float hours = minutes / 60f;
        float mins = minutes - (int)hours * 60f;
        string h = (int)hours < 10 ? "0" + ((int)hours).ToString() : ((int)hours).ToString();
        string m = (int)mins < 10 ? "0" + ((int)mins).ToString() : ((int)mins).ToString();
        float secs = sec - (int)mins * 60 - (int)hours * 60 * 60;
        string s = (int)secs < 10 ? "0" + ((int)secs).ToString() : ((int)secs).ToString();
        return h + ":" + m + ":" + s;
    }
}

[System.Serializable]
public struct FinishPanelStruct
{
    public GameObject panel;
    public Text playerPosTxt;
    public Text raceTime;
}