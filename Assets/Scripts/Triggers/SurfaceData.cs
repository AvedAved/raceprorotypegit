using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurfaceData : MonoBehaviour
{
    public SurfaceType type;
    public TiresType requiredTires;
}

public enum SurfaceType
{
    WetAsphalt,
    OffRoad,
    DryAsphalt
}