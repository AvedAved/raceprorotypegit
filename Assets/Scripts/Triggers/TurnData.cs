using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnData : MonoBehaviour
{
    public TurningSide turnSide;
    public bool CheckOnlyOneTime = false;
    [HideInInspector] public List<CarController> carsEntered = new List<CarController>();
    [HideInInspector] public List<CarController> carsExited = new List<CarController>();

    public float angle;
    public Transform point1, point2, fixedPoint;

    private void Start()
    {

        Vector3 v1 = fixedPoint.localPosition - point1.localPosition;
        Vector3 v2 = fixedPoint.localPosition - point2.localPosition;
        angle = Vector2.Angle(new Vector2(v1.x, v1.z), new Vector2(v2.x, v2.z));
    }

    public float GetMaxTurnSpeed(CarMove carMove)
    {
        float angleRatio = angle / 180f;
        float speedRatio = angleRatio * carMove.carParameters.maneuverability / carMove.carParameters.maxManeuverability;
        float maxSpeed = Mathf.Clamp(carMove.carParameters.maxGameSpeed * speedRatio, 0, carMove.carParameters.maxCarSpeed);
        return maxSpeed;
    }


    private void OnDrawGizmos()
    {
        if (point1 && point2 && fixedPoint)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(fixedPoint.position, point1.position);
            Gizmos.DrawLine(fixedPoint.position, point2.position);
        }
    }
}

public enum TurningSide
{
    RightTurn,
    LeftTurn
}