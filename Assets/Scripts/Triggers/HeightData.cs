using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeightData : MonoBehaviour
{
    public float angle;
    [SerializeField] private Transform point1, point2, fixedPoint;

    [Space(10)]
    public CurveType type;

    private void Start()
    {
        Vector3 v1 = fixedPoint.localPosition - point1.localPosition;
        Vector3 v2 = fixedPoint.localPosition - point2.localPosition;
        angle = Vector3.Angle(v1,v2);
    }

    public float GetMaxSpeed(CarMove carMove)
    {
        float angleRatio = angle / 180f;
        float speedRatio = angleRatio * carMove.carParameters.patency / carMove.carParameters.maxPatency;
        float maxSpeed = Mathf.Clamp(carMove.carParameters.maxGameSpeed * speedRatio, 0, carMove.carParameters.maxCarSpeed);
        return maxSpeed;
    }

    private void OnDrawGizmos()
    {
        if (point1 && point2 && fixedPoint)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(fixedPoint.position, point1.position);
            Gizmos.DrawLine(fixedPoint.position, point2.position);
        }
    }
}

public enum CurveType
{
    Straight,
    Climb,
    Descent
}