using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PathCreation;
using System;
using UnityEngine.SceneManagement;

public class RaceSystem : MonoBehaviour
{
    public static RaceSystem instance;

    public GameObject[] RaceTracks;
    [HideInInspector] public PathCreator currentPath;
    private PathActivator currentPathActivator;
    public CarMove playerCar;
    [SerializeField] private CarMove[] aiCars;

    private UISystem uiSystem => UISystem.instance;

    public static bool RaceStarted;

    [SerializeField] private GameObject StartBut, StopBut;
    [SerializeField] private Text timerTxt,bestTimeTxt;
    [SerializeField] private RectTransform playerArrow;

    private Coroutine raceCor;
    private Camera mainCam;
    private List<CarMove> allCars = new List<CarMove>();

    public float savedBestTime => PlayerPrefs.GetFloat("BestTime" + TrackNum.ToString(), 10000);
    [HideInInspector] public float currentTime;

    public static int TrackNum;
    public static int PlayerRacePosition;

    private bool testDrive => TestDriveManager.TrackNum != -1;

    private void Awake()
    {
        Time.timeScale = 1;
        instance = this;
        RaceStarted = false;
        if (PathActivator.instance == null)
        {
            SetTrack(Instantiate(RaceTracks[TrackNum]).GetComponent<PathActivator>());
        }
        else
        {
            SetTrack(PathActivator.instance);
        }
        InitAllCars();
        PlayerRacePosition = 0;
        GarageSystem.instance.playerCarParameters = playerCar.carParameters;
    }

    private void Start()
    {
        if (TestDriveManager.wasInTestDrive)
        {
            TestDriveManager.wasInTestDrive = false;
            uiSystem.OpenTestDrivePanelAtStart();
            TutorialManager.instance.StartTutorial(TutorialSpecialType.FirstTestDriveComplete);
        }
    }

    public void StartRace()
    {
        if (RaceStarted) return;
        RaceStarted = true;

        StartCoroutine(StartRaceIenum(1f));
    }

    IEnumerator StartRaceIenum(float delay)
    {
        playerCar.CalculateRatios();
        foreach (CarMove car in aiCars)
        {
            car.CalculateRatios();
        }
        uiSystem.MenuUIActive(false);
        uiSystem.GameUIActive(true);

        if (testDrive)
        {
            yield return new WaitForSeconds(0.5f);
        }

        CameraController.instance.StartFollow();

        yield return new WaitForSeconds(delay);

        playerCar.canMove = true;
        playerCar.SpeedUp();
        foreach (CarMove car in aiCars)
        {
            if (car.gameObject.activeSelf)
            {
                car.canMove = true;
                car.SpeedUp();
            }
        }
        raceCor = StartCoroutine(RaceIenum());
        SetButtonsActive(true);
    }

    public void StopRace()
    {
        SceneManager.LoadScene("Game");
    }

    public void ResetRace()
    {
        PlayerPrefs.DeleteAll();
        playerCar?.carParameters.ResetParameters();
        foreach (CarMove car in aiCars)
        {
            car.carParameters.ResetParameters();
        }
        PlayerPrefs.SetFloat("BestTime" + TrackNum.ToString(), 10000);
        SceneManager.LoadScene("Game");
    }
    public void FinishRace()
    {
        Time.timeScale = 1;
        RaceStarted = false;
        if (raceCor != null)
        {
            StopCoroutine(raceCor);
        }
        CameraController.instance.ResetPosition();
        playerArrow.gameObject.SetActive(false);

        uiSystem.GameUIActive(false);
        uiSystem.OpenFinishPanel(PlayerRacePosition,currentTime,testDrive);

        if (PlayerRacePosition == 1 && TrackNum == MenuCtrl.LastAvailableTrackID() && !testDrive)
        {
            MenuCtrl.SetTrackState(TrackNum, 2);
            MenuCtrl.SetTrackState(TrackNum+1, 1);
        }
    }

    public void SetTrack(PathActivator pathActivator)
    {
        if(currentPathActivator != null)
        {
            Destroy(currentPathActivator.gameObject);
        }
        currentPathActivator = pathActivator;
        currentPath = pathActivator.pathCreator ;

        playerCar.SetNewCarPosition(pathActivator.StartT.position,-5);
        playerCar.SetCarLocalPosition(pathActivator.carLocalXPos);
        pathActivator.cameraCtrl.Init(playerCar);
        mainCam = pathActivator.cameraCtrl.GetComponent<Camera>();

        switch (pathActivator.raceType)
        {
            case RaceType.OnlyPlayer:
                foreach (CarMove car in aiCars)
                {
                    car.gameObject.SetActive(false) ;
                }
                break;
            case RaceType.WithGhost:
                aiCars[0].SetNewCarPosition(playerCar.transform.position);
                aiCars[0].SetCarLocalPosition(-pathActivator.carLocalXPos);
                for(int i =1; i< aiCars.Length; i++)
                {
                    aiCars[i].gameObject.SetActive(false);
                }
                break;
            case RaceType.WithAI:
                aiCars[0].SetNewCarPosition(playerCar.transform.position);
                aiCars[0].SetCarLocalPosition(-pathActivator.carLocalXPos);
                aiCars[0].gameObject.SetActive(true);
                aiCars[1].SetNewCarPosition(playerCar.transform.position, -9);
                aiCars[1].SetCarLocalPosition(pathActivator.carLocalXPos);
                aiCars[1].gameObject.SetActive(true);
                aiCars[2].SetNewCarPosition(playerCar.transform.position, -9);
                aiCars[2].SetCarLocalPosition(-pathActivator.carLocalXPos);
                aiCars[2].gameObject.SetActive(true);
                break;
        }

        SetBestTimeText(savedBestTime < 5000 ? savedBestTime : 0);
        uiSystem.LapTxtUpdate(1);
    }

    public void ChangeTrack(int dir)
    {
        TrackNum = Mathf.Clamp(TrackNum + dir, 0, RaceTracks.Length - 1);
        SetTrack(Instantiate(RaceTracks[TrackNum]).GetComponent<PathActivator>());
    }

    public void BestTimeUpdate()
    {
        if (!testDrive)
        {
            if (currentTime < savedBestTime)
            {
                PlayerPrefs.SetFloat("BestTime" + TrackNum.ToString(), currentTime);
            }
        }
        else
        {
            if (currentTime < PlayerPrefs.GetFloat("BestTestDriveTime" + TestDriveManager.TrackNum.ToString(), 10000))
            {
                float newTime = currentTime;
                PlayerPrefs.SetFloat("BestTestDriveTime" + TestDriveManager.TrackNum.ToString(), newTime);
            }
        }
    }

    IEnumerator RaceIenum()
    {
        playerArrow.gameObject.SetActive(true);
        currentTime = 0;
        while (true)
        {
            currentTime += Time.deltaTime;
            timerTxt.text = $"ТЕКУЩЕЕ ВРЕМЯ \n {string.Format("{0:0.0}", currentTime)} с";

            Vector2 arrowPos = mainCam.WorldToScreenPoint(playerCar.carController.transform.position);
            arrowPos.y += 50;
            playerArrow.position = arrowPos;

            PlayerRacePosition = 1;

            foreach(CarMove aiCar in aiCars)
            {
                if(playerCar.distanceTravelled < aiCar.distanceTravelled)
                {
                    PlayerRacePosition++;
                }
            }
            uiSystem.PlayerRacePosTxtUpdate(PlayerRacePosition);
            yield return null;
        }
    }


    private void SetButtonsActive(bool race)
    {
        StartBut.SetActive(!race);
        StopBut.SetActive(race);
    }

    private void InitAllCars()
    {
        allCars.Add(playerCar);
        foreach(CarMove car in aiCars)
        {
            if (car.gameObject.activeSelf)
            {
                allCars.Add(car);
            }
        }
    }

    private void SetBestTimeText(float val)
    {
        bestTimeTxt.text = $"ЛУЧШЕЕ ВРЕМЯ \n {string.Format("{0:0.0}", val)} с";
    }
}

public enum RaceType
{
    OnlyPlayer,
    WithGhost,
    WithAI
}