using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DetailData : MonoBehaviour
{
    public DetailType type;
    public float value;

    public TiresType tiresType;

    public float requiredDriverAttention = 10f;

    public Text valueTxt,requiredAttentionTxt, parameterTxt;
    public Image InstalledImg, SelectedImg;

    [HideInInspector] public int detailID;
    [HideInInspector] public Button button;


    public void Init(int id)
    {
        detailID = id;
        button = GetComponent<Button>();

        if (type == DetailType.Tire)
        {
            valueTxt.gameObject.SetActive(false);
            parameterTxt.text = GarageSystem.TireNames[(int)tiresType];
        }
        else
        {
            valueTxt.text = $"{string.Format("{0:0}", value)}";
            parameterTxt.text = GarageSystem.ParameterNames[(int)type];
        }

        requiredAttentionTxt.text = requiredDriverAttention.ToString();
    }

}

public enum DetailType
{
    Engine,
    Transmission,
    Brakes,
    Steering,
    Suspension,
    Turbo,
    Tire
}