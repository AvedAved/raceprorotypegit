using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DetailsPanelData : MonoBehaviour
{
    [SerializeField] private Transform DetailsHolder;

    [HideInInspector] public List<DetailData> details = new List<DetailData>();

    [HideInInspector] public int selectedDetailID;
    [HideInInspector] public int installedDetailID;

 

    public void Init(int panID)
    {
        installedDetailID = PlayerPrefs.GetInt(nameof(installedDetailID) + panID.ToString(), 0);
        for(int i = 0; i < DetailsHolder.childCount; i++)
        {
            DetailData detailData = DetailsHolder.GetChild(i).GetComponent<DetailData>();
            if(detailData != null)
            {
                int id = details.Count;
                detailData.Init(id);
                detailData.button.onClick.AddListener(delegate { SelectDetail(id);});
                details.Add(detailData);
            }
        }

        GarageSystem.instance.InitInstalledDetail(details[installedDetailID]);

        ShowInstalledDetail(installedDetailID);
    }

    

    public void SelectDetail(int id)
    {
        selectedDetailID = id;
        if (selectedDetailID != installedDetailID)
        {
            ShowSelectedDetail(selectedDetailID);
            GarageSystem.instance.ShowDifferenceBetweenDetails(details[selectedDetailID]);
        }
    }
    
    public void ShowInstalledDetail(int id)
    {
        for(int i = 0; i< details.Count; i++)
        {
            details[i].InstalledImg.gameObject.SetActive(false);
        }
        details[id].InstalledImg.gameObject.SetActive(true) ;
    }

    private void ShowSelectedDetail(int id)
    {
        for (int i = 0; i < details.Count; i++)
        {
            details[i].SelectedImg.gameObject.SetActive(false);
        }
        details[id].SelectedImg.gameObject.SetActive(true);
    }

    public void SaveInstalledDetail(int panID)
    {
       PlayerPrefs.SetInt(nameof(installedDetailID) + panID.ToString(), installedDetailID);
    }

    
}
