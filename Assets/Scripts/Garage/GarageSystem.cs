using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GarageSystem : MonoBehaviour
{
    public GameObject MainPanel;
    [SerializeField] private GameObject DetailPanelsGO;
    [SerializeField] private Transform DetailPanelsContainer;

    private List<DetailsPanelData> DetailPanels = new List<DetailsPanelData>();

    public static GarageSystem instance;

    [HideInInspector] public int selectedDetailsPanelID;


    [HideInInspector] public DetailData installedEngine;
    [HideInInspector] public DetailData installedTransmission;
    [HideInInspector] public DetailData installedBrakes;
    [HideInInspector] public DetailData installedSteering;
    [HideInInspector] public DetailData installedSuspension;
    [HideInInspector] public DetailData installedTurbo;
    [HideInInspector] public DetailData installedTires;

    [SerializeField] private DetailDataStruct installedEngineData;
    [SerializeField] private DetailDataStruct installedTransmissionData;
    [SerializeField] private DetailDataStruct installedBrakesData;
    [SerializeField] private DetailDataStruct installedSteeringData;
    [SerializeField] private DetailDataStruct installedSuspensionData;
    [SerializeField] private DetailDataStruct installedTurboData;
    [SerializeField] private DetailDataStruct installedTiresData;

    [SerializeField] private DetailSprites[] detailSprites;

    public static string[] ParameterNames = new string[] { "МАКС.СКОР.", "УСКОРЕНИЕ", "ТОРМОЖЕНИЕ", "МАНЕВР.", "ПРОХОДИМОСТЬ", "ТЯГА" };
    public static string[] TireNames = new string[] { "СЛИКИ", "МИКСТ", "ДОЖДЕВЫЕ", "ВНЕДОРОЖНЫЕ" };

    [HideInInspector] public CarParameters playerCarParameters;

    public void Init()
    {
        instance = this;

        for(int i = 0; i < DetailPanelsContainer.childCount; i++)
        {
            DetailsPanelData detailsPanelData = DetailPanelsContainer.GetChild(i).GetComponent<DetailsPanelData>();
            if(detailsPanelData != null)
            {
                detailsPanelData.Init(DetailPanels.Count);
                DetailPanels.Add(detailsPanelData);
            }
        }
    }

    public void InstallDetail()
    {
        DetailsPanelData selectedDetailPanel = DetailPanels[selectedDetailsPanelID];
        DetailData detail = selectedDetailPanel.details[selectedDetailPanel.selectedDetailID];

        if (!CanInstallDetail(detail, playerCarParameters)) return;

        switch (detail.type)
        {
            case DetailType.Engine:
                playerCarParameters.maxSpeedStruct.slider.value = detail.value;
                installedEngine = detail;
                break;
            case DetailType.Transmission:
                playerCarParameters.accelerationStruct.slider.value = detail.value;
                installedTransmission = detail;
                break;
            case DetailType.Brakes:
                playerCarParameters.brakesStruct.slider.value = detail.value;
                installedBrakes = detail;
                break;
            case DetailType.Steering:
                playerCarParameters.maneuverStruct.slider.value = detail.value;
                installedSteering = detail;
                break;
            case DetailType.Suspension:
                playerCarParameters.patencyStruct.slider.value = detail.value;
                installedSuspension = detail;
                break;
            case DetailType.Turbo:
                playerCarParameters.rodStruct.slider.value = detail.value;
                installedTurbo = detail;
                break;
            case DetailType.Tire:
                playerCarParameters.tiresDropDown.value = (int)detail.tiresType;
                installedTires = detail;
                break;
        }
        selectedDetailPanel.installedDetailID = selectedDetailPanel.selectedDetailID;
        selectedDetailPanel.ShowInstalledDetail(selectedDetailPanel.installedDetailID);
        selectedDetailPanel.SaveInstalledDetail(selectedDetailsPanelID);

        UpdateInstalledDetailData(detail);
        playerCarParameters.DriverAttentionTxtUpdate();
        HideAllDifferences();
        detail.SelectedImg.gameObject.SetActive(false);
    }

    public void OpenDetailsPanel(int id)
    {
        selectedDetailsPanelID = id;
        for(int i = 0; i < DetailPanels.Count; i++)
        {
            DetailPanels[i].gameObject.SetActive(false);
        }
        DetailPanels[selectedDetailsPanelID].gameObject.SetActive(true);
        MainPanel.SetActive(false);
        DetailPanelsGO.SetActive(true);
    }

    public void DetailPanelsExit()
    {
        HideAllDifferences();
        MainPanel.SetActive(true);
        DetailPanelsGO.SetActive(false);
    }

    public void InitInstalledDetail(DetailData detail)
    {
        switch (detail.type)
        {
            case DetailType.Engine:
                installedEngine = detail;
                break;
            case DetailType.Transmission:
                installedTransmission = detail;
                break;
            case DetailType.Brakes:
                installedBrakes = detail;
                break;
            case DetailType.Steering:
                installedSteering = detail;
                break;
            case DetailType.Suspension:
                installedSuspension = detail;
                break;
            case DetailType.Turbo:
                installedTurbo = detail;
                break;
            case DetailType.Tire:
                installedTires = detail;
                break;
        }
        UpdateInstalledDetailData(detail);
    }

    private void UpdateInstalledDetailData(DetailData detail)
    {
        DetailDataStruct detailStruct = new DetailDataStruct();

        switch (detail.type)
        {
            case DetailType.Engine:
                detailStruct = installedEngineData;
                break;
            case DetailType.Transmission:
                detailStruct = installedTransmissionData;
                break;
            case DetailType.Brakes:
                detailStruct = installedBrakesData;
                break;
            case DetailType.Steering:
                detailStruct = installedSteeringData;
                break;
            case DetailType.Suspension:
                detailStruct = installedSuspensionData;
                break;
            case DetailType.Turbo:
                detailStruct = installedTurboData;
                break;
            case DetailType.Tire:
                detailStruct = installedTiresData;
                break;
        }

        detailStruct.icon.sprite = detailSprites[(int)detail.type].sprites[detail.detailID];
        if (detail.type == DetailType.Tire)
        {
            detailStruct.valueTxt.gameObject.SetActive(false);
            detailStruct.parameterTxt.text = TireNames[(int)detail.tiresType];
        }
        else
        {
            detailStruct.valueTxt.text = $"{string.Format("{0:0}", detail.value)}";
            detailStruct.parameterTxt.text = ParameterNames[(int)detail.type];
        }
    }

    private bool CanInstallDetail(DetailData newDetail, CarParameters carParameters)
    {
        float availableDriverAttention = carParameters.maxDriverAttention - carParameters.usedDriverAttention;
        switch (newDetail.type)
        {
            case DetailType.Engine:
                availableDriverAttention += installedEngine.requiredDriverAttention;
                break;
            case DetailType.Transmission:
                availableDriverAttention += installedTransmission.requiredDriverAttention;
                break;
            case DetailType.Brakes:
                availableDriverAttention += installedBrakes.requiredDriverAttention;
                break;
            case DetailType.Steering:
                availableDriverAttention += installedSteering.requiredDriverAttention;
                break;
            case DetailType.Suspension:
                availableDriverAttention += installedSuspension.requiredDriverAttention;
                break;
            case DetailType.Turbo:
                availableDriverAttention += installedTurbo.requiredDriverAttention;
                break;
            case DetailType.Tire:
                availableDriverAttention += installedTires.requiredDriverAttention;
                break;
        }

        return availableDriverAttention >= newDetail.requiredDriverAttention;
    }

    public void ShowDifferenceBetweenDetails(DetailData selectedDetail)
    {
        HideAllDifferences();

        ParameterSliderStruct parameterStruct = new ParameterSliderStruct();
        float currentValue = 0;

        switch (selectedDetail.type)
        {
            case DetailType.Engine:
                parameterStruct = playerCarParameters.maxSpeedStruct;
                currentValue = playerCarParameters.maxCarSpeed;
                break;
            case DetailType.Transmission:
                parameterStruct = playerCarParameters.accelerationStruct;
                currentValue = playerCarParameters.acceleration;
                break;
            case DetailType.Brakes:
                parameterStruct = playerCarParameters.brakesStruct;
                currentValue = playerCarParameters.breakForce;
                break;
            case DetailType.Steering:
                parameterStruct = playerCarParameters.maneuverStruct;
                currentValue = playerCarParameters.maneuverability;
                break;
            case DetailType.Suspension:
                parameterStruct = playerCarParameters.patencyStruct;
                currentValue = playerCarParameters.patency;
                break;
            case DetailType.Turbo:
                parameterStruct = playerCarParameters.rodStruct;
                currentValue = playerCarParameters.rod;
                break;
        }

        if (parameterStruct.differenceImg == null) return;

        float diff = selectedDetail.value - currentValue;

        bool positive = selectedDetail.type == DetailType.Brakes ? diff < 0 : diff > 0;

        if (positive)
        {
            parameterStruct.differenceImg.color = Color.green;
            parameterStruct.differenceImg.rectTransform.pivot = new Vector2(0, 0.5f);
            parameterStruct.differenceValueTxt.color = Color.green;
            parameterStruct.differenceValueTxt.text = selectedDetail.type == DetailType.Brakes ? "-" : "+";
        }
        else
        {
            parameterStruct.differenceImg.color = Color.red;
            parameterStruct.differenceImg.rectTransform.pivot = new Vector2(1f, 0.5f);
            parameterStruct.differenceValueTxt.color = Color.red;
            parameterStruct.differenceValueTxt.text = selectedDetail.type == DetailType.Brakes ? "+" : "-";
        }

        parameterStruct.differenceValueTxt.text += (Mathf.Abs((int)diff)).ToString();

        float maxDiffImgSize = parameterStruct.slider.GetComponent<RectTransform>().sizeDelta.x;

        float currentPoint = playerCarParameters.GetValueBetween((parameterStruct.slider.minValue, parameterStruct.slider.maxValue),
            (0f, maxDiffImgSize), currentValue);

        float newPoint = playerCarParameters.GetValueBetween((parameterStruct.slider.minValue, parameterStruct.slider.maxValue),
            (0f, maxDiffImgSize), selectedDetail.value);

        parameterStruct.differenceImg.rectTransform.sizeDelta = new Vector2(Mathf.Abs(newPoint - currentPoint), parameterStruct.differenceImg.rectTransform.sizeDelta.y);

        parameterStruct.differenceImg.gameObject.SetActive(true);
        parameterStruct.differenceValueTxt.gameObject.SetActive(true);
    }

    public void HideAllDifferences()
    {
        playerCarParameters.driveAttentionStruct.differenceImg.gameObject.SetActive(false);
        playerCarParameters.driveAttentionStruct.differenceValueTxt.gameObject.SetActive(false);

        playerCarParameters.maxSpeedStruct.differenceImg.gameObject.SetActive(false);
        playerCarParameters.maxSpeedStruct.differenceValueTxt.gameObject.SetActive(false);

        playerCarParameters.accelerationStruct.differenceImg.gameObject.SetActive(false);
        playerCarParameters.accelerationStruct.differenceValueTxt.gameObject.SetActive(false);

        playerCarParameters.brakesStruct.differenceImg.gameObject.SetActive(false);
        playerCarParameters.brakesStruct.differenceValueTxt.gameObject.SetActive(false);

        playerCarParameters.maneuverStruct.differenceImg.gameObject.SetActive(false);
        playerCarParameters.maneuverStruct.differenceValueTxt.gameObject.SetActive(false);

        playerCarParameters.patencyStruct.differenceImg.gameObject.SetActive(false);
        playerCarParameters.patencyStruct.differenceValueTxt.gameObject.SetActive(false);

        playerCarParameters.rodStruct.differenceImg.gameObject.SetActive(false);
        playerCarParameters.rodStruct.differenceValueTxt.gameObject.SetActive(false);
    }
}

[System.Serializable]
public struct DetailDataStruct
{
    public Text parameterTxt;
    public Text valueTxt;
    public Text requiredAttentionTxt;
    public Image icon;
}

[System.Serializable]
public struct DetailSprites
{
    public Sprite[] sprites;
}