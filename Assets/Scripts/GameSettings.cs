using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameSettings : MonoBehaviour
{
    public InputField minSpeedInput, maxSpeedInput;
    public InputField minAccelInput, maxAccelInput;
    public InputField minBreakInput, maxBreakInput;
    public InputField minManeuverInput, maxManeuverInput;
    public InputField minPatencyInput, maxPatencyInput;
    public InputField minRodInput, maxRodInput;

    public CarParameters[] carParameters;

    private void Awake()
    {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 60;
    }

    public void OnMinSpeedChange()
    {
        float value = 0;
        if (float.TryParse(minSpeedInput.text, out value))
        {
            foreach(CarParameters car in carParameters)
            {
                car.maxSpeedStruct.slider.minValue = value;
            }
        }
    }
    public void OnMaxSpeedChange()
    {
        float value = 0;
        if (float.TryParse(maxSpeedInput.text, out value))
        {
            foreach (CarParameters car in carParameters)
            {
                car.maxSpeedStruct.slider.maxValue = value;
            }
        }
    }
    public void OnMinAccelChange()
    {
        float value = 0;
        if (float.TryParse(minAccelInput.text, out value))
        {
            foreach (CarParameters car in carParameters)
            {
                car.accelerationStruct.slider.minValue = value;
            }
        }
    }
    public void OnMaxAccelChange()
    {
        float value = 0;
        if (float.TryParse(maxAccelInput.text, out value))
        {
            foreach (CarParameters car in carParameters)
            {
                car.accelerationStruct.slider.maxValue = value;
            }
        }
    }
    public void OnMinBreakChange()
    {
        float value = 0;
        if (float.TryParse(minBreakInput.text, out value))
        {
            foreach (CarParameters car in carParameters)
            {
                car.brakesStruct.slider.minValue = value;
            }
        }
    }
    public void OnMaxBreakChange()
    {
        float value = 0;
        if (float.TryParse(maxBreakInput.text, out value))
        {
            foreach (CarParameters car in carParameters)
            {
                car.brakesStruct.slider.maxValue = value;
            }
        }
    }
    public void OnMinMeuverChange()
    {
        float value = 0;
        if (float.TryParse(minManeuverInput.text, out value))
        {
            foreach (CarParameters car in carParameters)
            {
                car.maneuverStruct.slider.minValue = value;
            }
        }
    }
    public void OnMaxManeuverChange()
    {
        float value = 0;
        if (float.TryParse(maxManeuverInput.text, out value))
        {
            foreach (CarParameters car in carParameters)
            {
                car.maneuverStruct.slider.maxValue = value;
            }
        }
    }
    public void OnMinPatencyChange()
    {
        float value = 0;
        if (float.TryParse(minPatencyInput.text, out value))
        {
            foreach (CarParameters car in carParameters)
            {
                car.patencyStruct.slider.minValue = value;
            }
        }
    }
    public void OnMaxPatencyChange()
    {
        float value = 0;
        if (float.TryParse(maxPatencyInput.text, out value))
        {
            foreach (CarParameters car in carParameters)
            {
                car.patencyStruct.slider.maxValue = value;
            }
        }
    }
    public void OnMinRodChange()
    {
        float value = 0;
        if (float.TryParse(minRodInput.text, out value))
        {
            foreach (CarParameters car in carParameters)
            {
                car.rodStruct.slider.minValue = value;
            }
        }
    }
    public void OnMaxRodChange()
    {
        float value = 0;
        if (float.TryParse(maxRodInput.text, out value))
        {
            foreach (CarParameters car in carParameters)
            {
                car.rodStruct.slider.maxValue = value;
            }
        }
    }
}
