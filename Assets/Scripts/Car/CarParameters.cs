using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CarParameters : MonoBehaviour
{
    public float maxCarSpeed => maxSpeedStruct.slider.value;
    public float maxGameSpeed => maxSpeedStruct.slider.maxValue;
    public float acceleration => accelerationStruct.slider.value;
    public float breakForce => brakesStruct.slider.value;
    public float maxBreakForce => brakesStruct.slider.maxValue;
    public float maneuverability => maneuverStruct.slider.value;
    public float maxManeuverability => maneuverStruct.slider.maxValue;
    public float patency => patencyStruct.slider.value;
    public float maxPatency => patencyStruct.slider.maxValue;
    public float rod => rodStruct.slider.value;
    public float maxRod => rodStruct.slider.maxValue;

    public TiresType tires => tiresTypes[tiresDropDown.value];
    private TiresType[] tiresTypes = new TiresType[] { TiresType.Slicks, TiresType.Mixt, TiresType.Rain, TiresType.OffRoad };

    public float maxDriverAttention => driveAttentionStruct.slider.value;
    public float usedDriverAttention =>
        GarageSystem.instance.installedEngine.requiredDriverAttention +
        GarageSystem.instance.installedTransmission.requiredDriverAttention +
        GarageSystem.instance.installedBrakes.requiredDriverAttention +
        GarageSystem.instance.installedSteering.requiredDriverAttention +
        GarageSystem.instance.installedTurbo.requiredDriverAttention +
        GarageSystem.instance.installedSuspension.requiredDriverAttention +
        GarageSystem.instance.installedTires.requiredDriverAttention;

    [Space(10)]
    public ParameterSliderStruct driveAttentionStruct;
    public ParameterSliderStruct maxSpeedStruct;
    public ParameterSliderStruct accelerationStruct;
    public ParameterSliderStruct brakesStruct;
    public ParameterSliderStruct maneuverStruct;
    public ParameterSliderStruct patencyStruct;
    public ParameterSliderStruct rodStruct;

    public Dropdown tiresDropDown;


    public float maxPathMoveSpeed => maxCarSpeed / 5f;

    private float savedMaxCarSpeed => PlayerPrefs.GetFloat(nameof(savedMaxCarSpeed) + ClassID,maxSpeedStruct.slider.minValue);
    private float savedAcceleration => PlayerPrefs.GetFloat(nameof(savedAcceleration) + ClassID, accelerationStruct.slider.minValue);
    private float savedBrakeForce => PlayerPrefs.GetFloat(nameof(savedBrakeForce) + ClassID, brakesStruct.slider.maxValue);
    private float savedManeuver => PlayerPrefs.GetFloat(nameof(savedManeuver) + ClassID, maneuverStruct.slider.minValue);
    private float savedPatency => PlayerPrefs.GetFloat(nameof(savedPatency) + ClassID, patencyStruct.slider.minValue);
    private float savedRod => PlayerPrefs.GetFloat(nameof(savedRod) + ClassID, rodStruct.slider.minValue);
    private int savedTires => PlayerPrefs.GetInt(nameof(savedTires) + ClassID, tiresDropDown.value);
    private float savedMaxDriverAttention => PlayerPrefs.GetFloat(nameof(savedMaxDriverAttention) + ClassID,driveAttentionStruct.slider.minValue);

    public SurfaceAndRequiredTires[] surfacesAndRequiredTires;

    [SerializeField] private string ClassID = "0";

    [HideInInspector] public List<CarMove> cars;

   
    public void Init()
    {
        maxSpeedStruct.slider.value = savedMaxCarSpeed;
        accelerationStruct.slider.value = savedAcceleration;
        brakesStruct.slider.value = savedBrakeForce;
        maneuverStruct.slider.value = savedManeuver;
        patencyStruct.slider.value = savedPatency;
        rodStruct.slider.value = savedRod;
        tiresDropDown.value = savedTires;
        driveAttentionStruct.slider.value = savedMaxDriverAttention;

        SliderValTxtUpdate(maxSpeedStruct.valueTxt, ((int)maxCarSpeed).ToString());
        SliderValTxtUpdate(accelerationStruct.valueTxt, ((int)acceleration).ToString());
        SliderValTxtUpdate(brakesStruct.valueTxt, string.Format("{0:0.0}", breakForce));
        SliderValTxtUpdate(maneuverStruct.valueTxt, ((int)maneuverability).ToString());
        SliderValTxtUpdate(patencyStruct.valueTxt, ((int)patency).ToString());
        SliderValTxtUpdate(rodStruct.valueTxt, ((int)rod).ToString());
        DriverAttentionTxtUpdate();
    }

    public void ResetParameters()
    {
        PlayerPrefs.SetFloat(nameof(savedMaxCarSpeed) + ClassID, maxSpeedStruct.slider.minValue);
        PlayerPrefs.SetFloat(nameof(savedAcceleration) + ClassID, accelerationStruct.slider.minValue);
        PlayerPrefs.SetFloat(nameof(savedBrakeForce) + ClassID, brakesStruct.slider.maxValue);
        PlayerPrefs.SetFloat(nameof(savedManeuver) + ClassID, maneuverStruct.slider.minValue);
        PlayerPrefs.SetFloat(nameof(savedPatency) + ClassID, patencyStruct.slider.minValue);
        PlayerPrefs.SetFloat(nameof(savedRod) + ClassID, rodStruct.slider.minValue);
        PlayerPrefs.SetInt(nameof(savedTires) + ClassID, 0);
        PlayerPrefs.SetFloat(nameof(savedMaxDriverAttention) + ClassID, driveAttentionStruct.slider.minValue);
    }

    public void OnMaxSpeedChange()
    {
        PlayerPrefs.SetFloat(nameof(savedMaxCarSpeed) + ClassID, maxSpeedStruct.slider.value);
        SliderValTxtUpdate(maxSpeedStruct.valueTxt, ((int)maxCarSpeed).ToString());
    }

    public void OnAccelerationChange()
    {
        PlayerPrefs.SetFloat(nameof(savedAcceleration) + ClassID, accelerationStruct.slider.value);
        SliderValTxtUpdate(accelerationStruct.valueTxt, ((int)acceleration).ToString());
    }

    public void OnBreakForceChange()
    {
        PlayerPrefs.SetFloat(nameof(savedBrakeForce) + ClassID,brakesStruct.slider.value);
        SliderValTxtUpdate(brakesStruct.valueTxt, string.Format("{0:0.0}", breakForce));
    }

    public void OnManeuverChange()
    {
        PlayerPrefs.SetFloat(nameof(savedManeuver) + ClassID, maneuverStruct.slider.value);
        SliderValTxtUpdate(maneuverStruct.valueTxt, ((int)maneuverability).ToString());
    }

    public void OnPatencyChange()
    {
        PlayerPrefs.SetFloat(nameof(savedPatency) + ClassID, patencyStruct.slider.value);
        SliderValTxtUpdate(patencyStruct.valueTxt, ((int)patency).ToString());
    }

    public void OnRodChange()
    {
        PlayerPrefs.SetFloat(nameof(savedRod) + ClassID, rodStruct.slider.value);
        SliderValTxtUpdate(rodStruct.valueTxt, ((int)rod).ToString());
    }

    public void OnTiresChange()
    {
        PlayerPrefs.SetInt(nameof(savedTires) + ClassID, tiresDropDown.value);
        for (int i = 0; i< cars.Count; i++)
        {
            cars[i].SetSurfaceRatio(PathActivator.instance.generalSurfaceType);
        }
    }

    public void OnDriverAttentionChange()
    {
        PlayerPrefs.SetFloat(nameof(savedMaxDriverAttention) + ClassID, driveAttentionStruct.slider.value);
        DriverAttentionTxtUpdate();
    }

    private void SliderValTxtUpdate(Text text, string val)
    {
        text.text = val;
    }

    public void DriverAttentionTxtUpdate()
    {
        driveAttentionStruct.valueTxt.text = $"{usedDriverAttention} / {maxDriverAttention}";
    }

    public float GetValueBetween((float, float) vals1, (float, float) vals2, float currVal)
    {
        //vals1 это пределы, в которых находится currVal
        float delta1 = vals1.Item2 - vals1.Item1;
        float currValue = currVal - delta1;
        float ratio1 = delta1 / currValue;
        float result = (vals2.Item2 - vals2.Item1) / ratio1 + vals2.Item1;
        return result;
    }

    public float GetInverseValueBetween((float, float) vals1, (float, float) vals2, float currVal)
    {
        float delta1 = vals1.Item2 - vals1.Item1;
        float currValue = Mathf.Abs(currVal - vals1.Item2);
        float ratio1 = delta1 / currValue;
        float result = (vals2.Item2 - vals2.Item1) / ratio1 + vals2.Item1;
        return result;
    }
}

public enum TiresType
{
    Slicks,
    Mixt,
    Rain,
    OffRoad
}

[System.Serializable]
public struct ParameterSliderStruct
{
    public Slider slider;
    public Text valueTxt;
    public Image differenceImg;
    public Text differenceValueTxt;
}

[System.Serializable]
public struct SurfaceAndRequiredTires
{
    public SurfaceType surface;
    public List<TiresType> requiredTires;
}