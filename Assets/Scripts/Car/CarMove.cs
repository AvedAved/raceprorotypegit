using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;

public class CarMove : MonoBehaviour
{
    public bool PlayerCar;
    private PathCreator pathCreator => RaceSystem.instance.currentPath;
    [HideInInspector] public float distanceTravelled;
    EndOfPathInstruction end = EndOfPathInstruction.Loop;

    [SerializeField] private SpeedometerCtrl speedometerCtrl;
    public CarParameters carParameters;
    public CarController carController;
    public Transform CamFollowT;

    [HideInInspector] public bool canMove;
    [HideInInspector] public float currentSpeed;
    [HideInInspector] public float currentTargetSpeed;
    [HideInInspector] public float accelRatio;
    [HideInInspector] public bool canSpeedUp;
    [HideInInspector] public float surfaceRatio;

    public float convertedToPathSpeed => currentSpeed / carParameters.maxCarSpeed * carParameters.maxPathMoveSpeed;

    private Coroutine speedCor;
   

    private void Start()
    {
        carParameters.Init();
        if (!carParameters.cars.Contains(this))
        {
            carParameters.cars.Add(this);
        }
        canSpeedUp = false;
        CalculateRatios();
    }

    private void Update()
    {
        if (canMove)
        {
            if(canSpeedUp && currentSpeed < currentTargetSpeed)
            {
                CalculateAccelRatio();

                currentSpeed += carParameters.acceleration * accelRatio * surfaceRatio * Time.deltaTime;
            }

            distanceTravelled += convertedToPathSpeed * Time.deltaTime;
            SetCarPosition();
            speedometerCtrl?.SpeedUpdate();
        }
    }

    public void SpeedUp()
    {
        if (!canMove) return;

        StopSpeedChange();
        canSpeedUp = true;
    }

    public void SpeedDown(float breakTime, float targetSpeed)
    {
        if (!canMove) return;

        StopSpeedChange();
        canSpeedUp = false;
        speedCor = StartCoroutine(SpeedDownIenum(breakTime, targetSpeed));
    }

    IEnumerator SpeedDownIenum(float brakeTime, float targetSpeed)
    {
        speedometerCtrl?.SetBrakeIndicatorColor(1f);

        float delta = Mathf.Abs(currentSpeed - targetSpeed);
        float deltaSpeed = delta / brakeTime;
        while (currentSpeed > targetSpeed)
        {
            currentSpeed -= deltaSpeed * Time.deltaTime;
            yield return null;
        }
        speedometerCtrl?.SetBrakeIndicatorColor(0.4f);
        if (!carController.inTrigger)
        {
            yield return new WaitForSeconds(Mathf.Clamp(brakeTime,0.2f,1.5f));
            SpeedUp();
        }
    }

    private void CalculateAccelRatio()
    {
        float angle = transform.eulerAngles.x;
        if (angle >= 180f) angle -= 360f;

        if (angle < -1f)
        {
            //up
            float angleRatio = (180f + angle) / 180f;
            accelRatio = angleRatio * carParameters.rod / carParameters.maxRod;
        }
        else if (angle > 1f)
        {
            //down
            accelRatio = 180f / (180f - angle);
        }
        else accelRatio = 1;
    }

    public void SetSurfaceRatio(SurfaceType currentSurface)
    {
        for(int i = 0; i < carParameters.surfacesAndRequiredTires.Length; i++)
        {
            if(currentSurface == carParameters.surfacesAndRequiredTires[i].surface)
            {
                if (carParameters.surfacesAndRequiredTires[i].requiredTires.Contains(carParameters.tires))
                {
                    surfaceRatio = 1f;
                }
                else
                {
                    surfaceRatio = 0.9f;
                }
                break;
            }
        }
    }

    public void TargetSpeedUpdate(float newTargetSpeed)
    {
        currentTargetSpeed = newTargetSpeed;
    }

    public void CalculateRatios()
    {
        SetSurfaceRatio(PathActivator.instance.generalSurfaceType);
        TargetSpeedUpdate(carParameters.maxCarSpeed * surfaceRatio);
        accelRatio = 1f;
    }

    public void SetNewCarPosition(Vector3 newPos,float offset = 0)
    {
        distanceTravelled = pathCreator.path.GetClosestDistanceAlongPath(newPos) + offset;
        SetCarPosition();
    }

    private void StopSpeedChange()
    {
        if (speedCor != null)
        {
            StopCoroutine(speedCor);
        }
        speedometerCtrl?.SetBrakeIndicatorColor(0.4f);
    }

    public void FinishRace()
    {
        canMove = false;
        StopSpeedChange();
        canSpeedUp = false;
        currentSpeed = 0;
        speedometerCtrl?.SpeedUpdate();
        if (PlayerCar)
        {
            RaceSystem.instance.BestTimeUpdate();
            RaceSystem.instance.FinishRace();
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

    private void SetCarPosition()
    {
        transform.position = pathCreator.path.GetPointAtDistance(distanceTravelled, end);
        transform.rotation = Quaternion.LookRotation(pathCreator.path.GetDirectionAtDistance(distanceTravelled, end), Vector3.up);
    }

    public void SetCarLocalPosition(float xPos)
    {
        carController.transform.localPosition = new Vector3(xPos, carController.transform.localPosition.y, carController.transform.localPosition.z);
    }

    public Vector3 GetPosAlongPath(float distanceOffset)
    {
        return pathCreator.path.GetPointAtDistance(distanceTravelled + distanceOffset);
    }

    public float GetTravelledDistanceByPos(Vector3 pos)
    {
        return pathCreator.path.GetClosestDistanceAlongPath(pos);
    }
}
