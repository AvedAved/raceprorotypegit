using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CarController : MonoBehaviour
{    
    [SerializeField] private CarMove carMove;
    [SerializeField] private Transform carFront;
    [SerializeField] private Color gizmosColor;
    [SerializeField] private Transform carMesh;
    [SerializeField] private Transform ParticlesHolder;

    private float triggerCheckDist => carMove.carParameters.breakForce;
    private float triggerCheckRadius = 0.3f;
    private Collider currentTrigger,prevTrigger;
    private Vector3 triggerCheckPos;   
    private List<Collider> turnTriggersToPass = new List<Collider>();
    private List<Collider> highDownTriggersToPass = new List<Collider>();
    private List<Collider> surfaceTriggersToPass = new List<Collider>();
    private int lapCount;
    private ParticleSystem waterParticle, offroadParticle;
    private TrailRenderer waterTrail, offroadTrail;

    public bool inTurn => turnTriggersToPass.Count > 0;
    public bool inTrigger => turnTriggersToPass.Count > 0 || highDownTriggersToPass.Count > 0;

    [Header("Sensors")]
    [SerializeField] private Transform RightSensorsHolder, LeftSensorsHolder;
    [SerializeField] private float sensorDistance = 4.5f;
    [SerializeField] private float sideSensorDistance = 6f;
    private Transform[]  leftSensorsPoints, rightSensorsPoints;

    [HideInInspector] public Lanes currentLane;
    [SerializeField] private float SpeedDifferenceForOvertaking = 10f;
    private bool tryingToChangeLane = false;
    private bool canChangeLane = true;
    private CarMove detectedCar;

    private void Start()
    {
        prevTrigger = null;
        InitSensorsPoints();
        InitLane();
        lapCount = -1;
        InitParticles();
    }

    private void Update()
    {
        if (PathActivator.instance.raceType == RaceType.WithAI)
        {
            CheckSensors();
        }
        CheckTriggers();
    }

    private void CheckSensors()
    {
       // if (tryingToChangeLane) return;

        RaycastHit hit;
        float detectDistance = 0;
        
        Debug.DrawRay(carFront.position, carFront.forward * (sensorDistance+1), Color.red);
        if (Physics.SphereCast(carFront.position,1f,carFront.forward, out hit, sensorDistance, 1 << 6))
        {
            detectedCar = hit.transform.parent.GetComponent<CarMove>();
            if (detectedCar == carMove) return;
            detectDistance = hit.distance;
        }
        else detectedCar = null;

        if (detectedCar == null)
        {
            carMove.canSpeedUp = true;
        }
        else
        {
            if (detectedCar.carController.currentLane != currentLane) return;
            
            float moveTime = detectDistance / carMove.convertedToPathSpeed;

            if (detectedCar.carController.inTurn)
            {
                TurnData lastTrigger = detectedCar.carController.turnTriggersToPass[detectedCar.carController.turnTriggersToPass.Count - 1].GetComponent<TurnData>();
                float potencialSpeed = lastTrigger.GetMaxTurnSpeed(carMove) * carMove.surfaceRatio;
                if(lastTrigger.turnSide == TurningSide.LeftTurn && currentLane == Lanes.LeftLane ||
                    lastTrigger.turnSide == TurningSide.RightTurn && currentLane == Lanes.RightLane)
                {
                    potencialSpeed *= 0.9f;
                }
                if(potencialSpeed - detectedCar.currentTargetSpeed >= SpeedDifferenceForOvertaking && !tryingToChangeLane && canChangeLane && AdjacentLaneIsFree())
                {
                    carMove.canSpeedUp = false;
                    carMove.TargetSpeedUpdate(potencialSpeed);
                    StartLaneChanging(moveTime);
                }
                else
                {
                    carMove.canSpeedUp = false;
                    carMove.TargetSpeedUpdate(detectedCar.currentTargetSpeed);
                    carMove.SpeedDown(moveTime, detectedCar.currentSpeed * 0.9f);
                }
            }
            else
            {
                if((carMove.currentTargetSpeed - detectedCar.currentTargetSpeed >= SpeedDifferenceForOvertaking ||
                    carMove.currentSpeed - detectedCar.currentSpeed >= SpeedDifferenceForOvertaking ||
                    carMove.accelRatio - detectedCar.accelRatio >= 0.1f )
                    && !tryingToChangeLane && canChangeLane && AdjacentLaneIsFree())
                {
                    carMove.canSpeedUp = false;
                    StartLaneChanging(moveTime);
                }
                else
                {
                    carMove.canSpeedUp = false;
                    carMove.TargetSpeedUpdate(detectedCar.currentTargetSpeed);
                    carMove.SpeedDown(moveTime, detectedCar.currentSpeed * 0.9f);
                }
            }
        }
    }

    private void StartLaneChanging(float moveTime)
    {
        if (tryingToChangeLane) return;

        tryingToChangeLane = true;
        canChangeLane = false;

        if (currentLane == Lanes.RightLane)
        {
            currentLane = Lanes.LeftLane;
            transform.DOLocalMoveX(-PathActivator.instance.carLocalXPos, moveTime).OnComplete(() =>
            {
                OnLaneChangingComplete();
            });
            carMesh.DOKill();
            carMesh.DOLocalRotateQuaternion(Quaternion.Euler(0, -20f, 0), moveTime / 2f).OnComplete(() =>
            {
                carMesh.DOLocalRotateQuaternion(Quaternion.Euler(0, 0, 0), moveTime / 2f);
            });
        }
        else if (currentLane == Lanes.LeftLane)
        {
            currentLane = Lanes.RightLane;
            transform.DOLocalMoveX(PathActivator.instance.carLocalXPos, moveTime).OnComplete(() =>
            {
                OnLaneChangingComplete();
            });
            carMesh.DOKill();
            carMesh.DOLocalRotateQuaternion(Quaternion.Euler(0, 20f, 0), moveTime / 2f).OnComplete(() =>
            {
                carMesh.DOLocalRotateQuaternion(Quaternion.Euler(0, 0, 0), moveTime / 2f);
            });
        }
    }
   
    private void OnLaneChangingComplete()
    {
        tryingToChangeLane = false;
        carMove.canSpeedUp = true;
        if (gameObject.activeSelf)
        {
            StartCoroutine(ChangeLaneDelay(1f));
        }
    }

    private IEnumerator ChangeLaneDelay(float delay)
    {
        canChangeLane = false;
        yield return new WaitForSeconds(delay);
        canChangeLane = true;
    }

    private bool AdjacentLaneIsFree()
    {
        Transform[] sidePoints = new Transform[] { };

        if (currentLane == Lanes.RightLane)
        {
            sidePoints = leftSensorsPoints;
        }
        else if (currentLane == Lanes.LeftLane)
        {
            sidePoints = rightSensorsPoints;
        }

        if (sidePoints == null || sidePoints.Length == 0) return true;

        Ray ray;
        RaycastHit hit;
        for (int i = 0; i < sidePoints.Length; i++)
        {
            ray = new Ray(sidePoints[i].position, sidePoints[i].forward);
            Debug.DrawRay(sidePoints[i].position, sidePoints[i].forward * sideSensorDistance, Color.red);
            if (Physics.Raycast(ray, out hit, sideSensorDistance, 1 << 6))
            {
                return false;
            }
        }
        return true;
    }

    private void CheckTriggers()
    {
        triggerCheckPos = carMove.GetPosAlongPath(triggerCheckDist);

        Collider[] triggers = Physics.OverlapSphere(triggerCheckPos, triggerCheckRadius, 1 << 7);
        currentTrigger = triggers.Length > 0 ? triggers[0] : null;
        if (currentTrigger != null && prevTrigger != currentTrigger)
        {
            prevTrigger = currentTrigger;
            float distance = Vector2.Distance(new Vector2(triggerCheckPos.x, triggerCheckPos.z), new Vector2(carFront.position.x, carFront.position.z));
            float brakeTime = Mathf.Clamp(distance / carMove.convertedToPathSpeed, 0.2f, 1f);
            float maxSpeedInTrigger = carMove.carParameters.maxCarSpeed * carMove.surfaceRatio;
            if (currentTrigger.CompareTag("Height"))
            {
                if (!highDownTriggersToPass.Contains(currentTrigger))
                {
                    highDownTriggersToPass.Add(currentTrigger);
                }
                HeightData heightData = currentTrigger.GetComponent<HeightData>();
                maxSpeedInTrigger = heightData.GetMaxSpeed(carMove) * carMove.surfaceRatio;
                carMove.TargetSpeedUpdate(maxSpeedInTrigger);
                if (maxSpeedInTrigger < carMove.currentSpeed)
                {
                    carMove.SpeedDown(brakeTime, maxSpeedInTrigger);
                }
                else
                {
                    carMove.SpeedUp();
                }
            }
            if (currentTrigger.CompareTag("Turn"))
            {
                if (!turnTriggersToPass.Contains(currentTrigger))
                {
                    turnTriggersToPass.Add(currentTrigger);
                }
                TurnData turnData = currentTrigger.GetComponent<TurnData>();
                if (turnData.CheckOnlyOneTime)
                {
                    if (turnData.carsEntered.Contains(this))
                    {
                        return;
                    }
                    turnData.carsEntered.Add(this);
                }
                maxSpeedInTrigger = turnData.GetMaxTurnSpeed(carMove) * carMove.surfaceRatio;
                carMove.TargetSpeedUpdate(maxSpeedInTrigger);

                if (PathActivator.instance.raceType == RaceType.WithAI &&
                    (turnData.turnSide == TurningSide.LeftTurn && currentLane == Lanes.RightLane ||
                        turnData.turnSide == TurningSide.RightTurn && currentLane == Lanes.LeftLane) )
                {
                    if (!tryingToChangeLane && AdjacentLaneIsFree())
                    {
                        StartLaneChanging(brakeTime);
                    }
                    else
                    {
                        maxSpeedInTrigger *= 0.9f;
                        carMove.TargetSpeedUpdate(maxSpeedInTrigger);
                    }
                }

                if (maxSpeedInTrigger < carMove.currentSpeed)
                {
                    carMove.SpeedDown(brakeTime, maxSpeedInTrigger);
                }
                else
                {
                    carMove.SpeedUp();
                }
            }

           
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Surface"))
        {
            if (!surfaceTriggersToPass.Contains(other))
            {
                surfaceTriggersToPass.Add(other);
            }
            SurfaceData surfaceData = other.GetComponent<SurfaceData>();
            carMove.TargetSpeedUpdate(carMove.currentTargetSpeed / carMove.surfaceRatio);
            carMove.SetSurfaceRatio(surfaceData.type);
            carMove.TargetSpeedUpdate(carMove.currentTargetSpeed * carMove.surfaceRatio);
            if(carMove.currentSpeed > carMove.currentTargetSpeed)
            {
                carMove.SpeedDown(0.2f, carMove.currentTargetSpeed);
            }
            else
            {
                carMove.SpeedUp();
            }
            PlayEffects(surfaceData.type);
        }
    }

    private void OnTriggerExit(Collider other)
    {

        if (other.CompareTag("Finish"))
        {
            lapCount = Mathf.Clamp(lapCount + 1,0, PathActivator.instance.lapsAmount);
            if (carMove.PlayerCar)
            {
                UISystem.instance.LapTxtUpdate(Mathf.Clamp(lapCount + 1, 1, PathActivator.instance.lapsAmount));
            }
            if(lapCount >= PathActivator.instance.lapsAmount)
            {
                carMove.FinishRace();
            }
        }

        if (surfaceTriggersToPass.Contains(other))
        {
            surfaceTriggersToPass.Remove(other);
        }
        if (other.CompareTag("Surface"))
        {
            PlayEffects(PathActivator.instance.generalSurfaceType);
            if(surfaceTriggersToPass.Count > 0)
            {
                return;
            }
            SurfaceData surfaceData = other.GetComponent<SurfaceData>();
            carMove.TargetSpeedUpdate(carMove.currentTargetSpeed / carMove.surfaceRatio);
            carMove.SetSurfaceRatio(PathActivator.instance.generalSurfaceType);
            carMove.TargetSpeedUpdate(carMove.currentTargetSpeed * carMove.surfaceRatio);
            if(carMove.currentSpeed > carMove.currentTargetSpeed)
            {
                carMove.SpeedDown(0.2f, carMove.currentTargetSpeed);
            }
            else
            {
                carMove.SpeedUp();
            }
        }

        if (highDownTriggersToPass.Contains(other))
        {
            highDownTriggersToPass.Remove(other);
        }
        if (turnTriggersToPass.Contains(other))
        {
            turnTriggersToPass.Remove(other);
        }
        if (other.CompareTag("Height") && currentTrigger == null)
        {
            if (highDownTriggersToPass.Count > 0 || turnTriggersToPass.Count > 0)
            {
                return;
            }
            HeightData heightData = other.GetComponent<HeightData>();
            carMove.TargetSpeedUpdate(carMove.carParameters.maxCarSpeed * carMove.surfaceRatio);
            carMove.SpeedUp();
        }
        if (other.CompareTag("Turn") && currentTrigger == null)
        {
            if (highDownTriggersToPass.Count > 0 || turnTriggersToPass.Count > 0)
            {
                return;
            }
            TurnData turnData = other.GetComponent<TurnData>();
            if (turnData.CheckOnlyOneTime)
            {
                if (turnData.carsExited.Contains(this))
                {
                    return;
                }
                turnData.carsExited.Add(this);
            }
            carMove.TargetSpeedUpdate(carMove.carParameters.maxCarSpeed * carMove.surfaceRatio);
            carMove.SpeedUp();
        }
    }

  

    private void InitLane()
    {
        float xPos = transform.localPosition.x;
        if (xPos >= 0.2f)
        {
            currentLane = Lanes.RightLane;
        } else if(xPos <= -0.2f)
        {
            currentLane = Lanes.LeftLane;
        } else
        {
            currentLane = Lanes.Center;
        }
    }

    private void InitSensorsPoints()
    {
        //sensorsPoints = new Transform[SensorsHolder.childCount];
        //for (int i = 0; i < sensorsPoints.Length; i++)
        //{
        //    sensorsPoints[i] = SensorsHolder.GetChild(i);
        //}

        leftSensorsPoints = new Transform[LeftSensorsHolder.childCount];
        for (int i = 0; i < leftSensorsPoints.Length; i++)
        {
            leftSensorsPoints[i] = LeftSensorsHolder.GetChild(i);
        }

        rightSensorsPoints = new Transform[RightSensorsHolder.childCount];
        for (int i = 0; i < rightSensorsPoints.Length; i++)
        {
            rightSensorsPoints[i] = RightSensorsHolder.GetChild(i);
        }
    }

    private void InitParticles()
    {
        Transform t = ParticlesHolder.GetChild(0);
        waterParticle = t.GetChild(0).GetComponent<ParticleSystem>();
        waterTrail = t.GetChild(1).GetComponent<TrailRenderer>();

        t = ParticlesHolder.GetChild(1);
        offroadParticle = t.GetChild(0).GetComponent<ParticleSystem>();
        offroadTrail = t.GetChild(1).GetComponent<TrailRenderer>();
    }

    private void PlayEffects(SurfaceType surface)
    {
        waterParticle.Stop();
        offroadParticle.Stop();
        waterTrail.emitting = false ;
        offroadTrail.emitting = false ;
        switch (surface)
        {
            case SurfaceType.WetAsphalt:
                waterParticle.Play();
                waterTrail.emitting = true ;
                break;
            case SurfaceType.OffRoad:
                offroadParticle.Play();
                offroadTrail.emitting = true;
                break;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = gizmosColor;
        Gizmos.DrawSphere(triggerCheckPos, triggerCheckRadius);
    }
}

public enum Lanes
{
    RightLane,
    LeftLane,
    Center
}