using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdditionalDialogData : MonoBehaviour
{
    public Text dialogTxt;
    public Text nextButtonTxt;
    public Button nextButton;
}
