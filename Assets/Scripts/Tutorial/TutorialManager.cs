using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class TutorialManager : MonoBehaviour
{
    [SerializeField] private int[] tutorialLoopLastID;
    [SerializeField] private TutorialStruct[] tutorialStructs;
    [SerializeField] private GameObject Background;
    [SerializeField] private GameObject DialogPanel;
    [SerializeField] private Text DialogText;
    [SerializeField] private Text CompleteButText;
    [SerializeField] private float delayBetweenTutorials = 0.3f;

    private int[] tutorialLoopCompleted; // 0 - not completed, 1 - completed
    private int currentTutorialID;

    private List<GameObject> clonedObjects = new List<GameObject>();

    public static TutorialManager instance;

    private void Awake()
    {
        instance = this;
        InitCompletedTutorials();
    }

    public void StartTutorial(TutorialSpecialType type)
    {
        TutorialStruct tutorialStruct = tutorialStructs.First(x => x.specialType == type);
        int id = tutorialStructs.ToList().IndexOf(tutorialStruct);

        StartTutorial(id);
    }

    public void StartTutorial(int id)
    {
        int loopID = -1;
        for(int i = 0; i < tutorialLoopLastID.Length; i++)
        {
            if(id <= tutorialLoopLastID[i])
            {
                loopID = i;
                break;
            }
        }

        if (id >= tutorialStructs.Length || tutorialLoopCompleted[loopID] == 1) return;

        StartCoroutine(StartTutorialIenum(id));     
    }

    private IEnumerator StartTutorialIenum(int id)
    {
        TutorialStruct tutorialStruct = tutorialStructs[id];
        float delay = tutorialStruct.WithDelay ? delayBetweenTutorials : 0f;
        if (!tutorialStruct.DialogOnly)
        {
            DialogPanel.SetActive(false);
            Background.SetActive(true);
            yield return new WaitForSeconds(delay);
            if (tutorialStruct.CompleteButton != null)
            {
                tutorialStruct.CompleteButton.onClick.AddListener(CompleteTutorial);
            }
            for (int i = 0; i < tutorialStruct.ObjectsToClone.Length; i++)
            {
                GameObject clone = Instantiate(tutorialStruct.ObjectsToClone[i], tutorialStruct.ObjectsToClone[i].transform.parent);
                clone.transform.SetParent(Background.transform, true);
                Button cloneBut = clone.GetComponent<Button>();
                if (cloneBut != null)
                {
                    Button originalBut = tutorialStruct.ObjectsToClone[i].GetComponent<Button>();
                    cloneBut.onClick = originalBut.onClick;
                    cloneBut.image.rectTransform.sizeDelta = originalBut.image.rectTransform.sizeDelta;
                    cloneBut.image.rectTransform.position = originalBut.image.rectTransform.position;
                }
                clonedObjects.Add(clone);
            }
            if (tutorialStruct.AdditionalDialog != null)
            {
                tutorialStruct.AdditionalDialog.dialogTxt.text = tutorialStruct.dialogText;
                if (!string.IsNullOrEmpty(tutorialStruct.completeButText))
                {
                    tutorialStruct.AdditionalDialog.nextButtonTxt.text = tutorialStruct.completeButText;
                    tutorialStruct.AdditionalDialog.nextButton.gameObject.SetActive(true);
                }
                else
                {
                    tutorialStruct.AdditionalDialog.nextButton.gameObject.SetActive(false);
                }
                tutorialStruct.AdditionalDialog.gameObject.SetActive(true);
            }
        }
        else
        {
            yield return new WaitForSeconds(delay);
            Background.SetActive(false);
            DialogText.text = tutorialStruct.dialogText;
            CompleteButText.text = tutorialStruct.completeButText;
            DialogPanel.SetActive(true);
        }
        currentTutorialID = id;
        DoSpecialStartAction();
    }

    public void CompleteTutorial()
    {
        Debug.Log("completed " + currentTutorialID);
        //tutorialCompleted[currentTutorialID] = 1;
        //PlayerPrefs.SetInt(nameof(tutorialCompleted) + currentTutorialID.ToString(), 1);
        for(int i = 0; i < tutorialLoopLastID.Length; i++)
        {
            if(currentTutorialID == tutorialLoopLastID[i])
            {
                tutorialLoopCompleted[i] = 1;
                PlayerPrefs.SetInt(nameof(tutorialLoopCompleted) + i.ToString(), 1);
            }
        }
        foreach(GameObject go in clonedObjects)
        {
            Destroy(go);
        }
        clonedObjects.Clear();

        Background.SetActive(false);
        DialogPanel.SetActive(false);

        if(tutorialStructs[currentTutorialID].AdditionalDialog != null)
        {
            tutorialStructs[currentTutorialID].AdditionalDialog.gameObject.SetActive(false);
        }
        if(tutorialStructs[currentTutorialID].CompleteButton != null)
        {
            tutorialStructs[currentTutorialID].CompleteButton.onClick.RemoveListener(CompleteTutorial);
        }
        if (tutorialStructs[currentTutorialID].StartNextTutor)
        {
            StartTutorial(currentTutorialID+1);
        }
    }

    private void InitCompletedTutorials()
    {
        tutorialLoopCompleted = new int[tutorialLoopLastID.Length];
        for(int i = 0; i < tutorialLoopCompleted.Length; i++)
        {
            tutorialLoopCompleted[i] = PlayerPrefs.GetInt(nameof(tutorialLoopCompleted) + i.ToString(), 0);
        }
    }

    private void DoSpecialStartAction()
    {
        switch (currentTutorialID)
        {
            case 0:
                break;
            case 1:
                break;
            case 2:
                break;
            case 3:
                clonedObjects[0].GetComponent<Button>().onClick.RemoveAllListeners();
                break;
            case 6:
                Background.SetActive(false);
                break;
        }
    }

    public void CompleteTutorialLoop(int id)
    {
        if(tutorialLoopCompleted[id] == 0)
        {
            tutorialLoopCompleted[id] = 1;
            PlayerPrefs.SetInt(nameof(tutorialLoopCompleted) + id.ToString(), 1);
        }
    }
 
}

[System.Serializable]
public struct TutorialStruct
{
    public GameObject[] ObjectsToClone;
    public Button CompleteButton;
    public AdditionalDialogData AdditionalDialog;
    public bool DialogOnly;
    [TextArea]
    public string dialogText;
    [TextArea]
    public string completeButText;
    public bool StartNextTutor;
    public bool WithDelay;
    public TutorialSpecialType specialType;
}

public enum TutorialSpecialType
{
    None,
    FirstGameSceneLoad,
    FirstTestDriveComplete,
    FirstGarageOpen
}