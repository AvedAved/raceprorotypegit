using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CameraController : MonoBehaviour
{
    public static CameraController instance;

    [SerializeField] private float dampTime = 0.2f;
    [HideInInspector] public Transform camFollowT,carT;
    private bool canFollow;
    private Vector3 offset;

    private float margin = 0.1f;
    public static bool MustFollow;
    private Vector3 startPos;
    private Quaternion startRot;

    private void Awake()
    {
        instance = this;
        MustFollow = true;
    }

    public void Init(CarMove car)
    {
        camFollowT = car.CamFollowT;
        carT = car.transform;
        startPos = transform.position;
        startRot = transform.rotation;
    }

    private void Update()
    {
        if (canFollow && MustFollow)
        {
            float targetX = carT.position.x - offset.x;
            float targetY = carT.position.y - offset.y;
            float targetZ = carT.position.z - offset.z;

            if (Mathf.Abs(transform.position.x - targetX) > margin)
            {
                targetX = Mathf.Lerp(transform.position.x, targetX, 1f / dampTime * Time.deltaTime);
            }

            if (Mathf.Abs(transform.position.y - targetY) > margin)
            {
                targetY = Mathf.Lerp(transform.position.y, targetY, 1f / dampTime * Time.deltaTime);
            }

            if (Mathf.Abs(transform.position.z - targetZ) > margin)
            {
                targetZ = Mathf.Lerp(transform.position.z, targetZ, 1f / dampTime * Time.deltaTime);
            }

            transform.position = new Vector3(targetX, targetY, targetZ);
        }
    }

    public void StartFollow()
    {
        if (!MustFollow) return;
        StartCoroutine(FollowIenum());
    }

    IEnumerator FollowIenum()
    {
        float speed = 1f;
        transform.DOMove(camFollowT.position, speed);
        transform.DORotateQuaternion(camFollowT.rotation, speed);
        offset = carT.position - camFollowT.position;
        yield return new WaitForSeconds(speed + 0.1f);
        canFollow = true;
    }

    public void ResetPosition()
    {
        canFollow = false;
        transform.position = startPos;
        transform.rotation = startRot;
    }
}
