using UnityEngine;
using PathCreation.Examples;
using PathCreation;

public class PathActivator : MonoBehaviour
{
    public RaceType raceType;
    public int lapsAmount = 1;
    public float carLocalXPos = 1.8f;
    public PathCreator pathCreator;
    [SerializeField] private RoadMeshCreator roadMesh;
    [SerializeField] private Material roadMaterial;

    public static PathActivator instance;

    public CameraController cameraCtrl;
    public Transform StartT;

    public SurfaceType generalSurfaceType = SurfaceType.DryAsphalt;
    public WeatherType weatherType;

    private string[] WeatherTexts = new string[] {"СОЛНЕЧНО","ДОЖДЛИВО"};
    private string[] SurfaceTexts = new string[] { "ЛУЖИ", "БЕЗДОРОЖЬЕ", "АСФАЛЬТ"};

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        roadMesh?.TriggerUpdate();
        roadMaterial.mainTextureScale = new Vector2(1, 200);

        UISystem.instance.WeatherTxt.text = WeatherTexts[(int)weatherType];
        UISystem.instance.LapsAmountTxt.text = lapsAmount.ToString();
        UISystem.instance.SurfaceTxt.text = SurfaceTexts[(int)generalSurfaceType];
    }
}

public enum WeatherType
{
    Sunny,
    Rainy
}