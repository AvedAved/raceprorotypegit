using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuCtrl : MonoBehaviour
{
    [SerializeField] private Transform TrackButtonsHolder;
    [SerializeField] private Sprite[] TrackButtonStateSprites;

    private List<Button> trackButtons = new List<Button>();

    public static int selectedTrackNum;

    public static int[] trackStates; // 0 - closed, 1 - available, 2 - done

    [SerializeField] private Text stadiumNameTxt;
    [SerializeField] private string[] stadiumNames;


    private void Awake()
    {
        InitTrackButtons();
    }

    private void Start()
    {
        selectedTrackNum = -1;
        TutorialManager.instance.StartTutorial(0);

    }

    private void SelectTrack(int id)
    {
        if (trackStates[id] == 0) return;

        selectedTrackNum = id;

        for(int i = 0; i < trackButtons.Count; i++)
        {
            trackButtons[i].transform.GetChild(0).gameObject.SetActive(selectedTrackNum == i);
        }

        stadiumNameTxt.text = stadiumNames[selectedTrackNum];
    }

    public void LoadGame()
    {
        if (selectedTrackNum == -1 || trackStates[selectedTrackNum] == 0) return;

        RaceSystem.TrackNum = selectedTrackNum;

        TutorialManager.instance.CompleteTutorialLoop(0);

        SceneManager.LoadScene("Game");
    }

    private void InitTrackButtons()
    {
        trackStates = new int[TrackButtonsHolder.childCount];

        for (int i = 0; i < TrackButtonsHolder.childCount; i++)
        {
            trackStates[i] = PlayerPrefs.GetInt(nameof(trackStates) + i, i == 0 ? 1 : 0);
            Button but = TrackButtonsHolder.GetChild(i).GetComponent<Button>();
            if (but != null)
            {
                int id = trackButtons.Count;
                but.onClick.AddListener(delegate { SelectTrack(id); } );
                trackButtons.Add(but);
            }
        }

        TrackButtonsStateUpdate();
    }

    private void TrackButtonsStateUpdate()
    {
        for(int i = 0; i < trackButtons.Count; i++)
        {
            trackButtons[i].image.sprite = TrackButtonStateSprites[trackStates[i]];
        }
    }

    public static void SetTrackState(int trackID,int state)
    {
        if (trackStates == null || trackID > trackStates.Length - 1) return;

        trackStates[trackID] = state;
        PlayerPrefs.SetInt(nameof(trackStates) + trackID, state);
    }

    public static int LastAvailableTrackID()
    {
        if (trackStates == null) return 0;

        for(int i = 0; i < trackStates.Length; i++)
        {
            if(trackStates[i] == 0)
            {
                return i-1;
            }
        }
        return 0;
    }
}
